# Session 2

- Welcome back. 

## Theory

<!--### Any doubts ????-->

- How to learn any supervised learning model ?
- Ask yourself these jars ....

### Images

- Array of numbers
- 3D array
- Length, Breadth, Depth
- RGB Image   
- Blue pixel
- Pixel
- (0, 0, 255)
- (255 ,255, 255)
- White
- (0, 0, 0)
- Black
- (255, 255, 0)
- Yellow 

- (1280, 720, 3)

### Data Jar: -
- Any supervised: -
- Numbers.

### Model Jar: -
- Model
- Assumptions
- Whether assumptions are good or not ?
- Testing. 


### Task Jar
- Regression or Classification

- Classification
Cat vs Dog Binary Classification

Cat vs Dog vs Frog vs Human
Mutliclass Classifcation.


### Learning Algorithm Jar: -
- Parameters
Variable assumptions that were made.

- Learn them
Differs from model to model.

In general 
1. Learn the paremeters


### Cost/Loss functionJar
- Supervised Learning
- Guide to your learning algorithm.
- Value. Decide whether learning is proper or not.
- We are on right track.

### Evaluation Jar
- Define whether model is good / bad. Right or wrong.

Model specific. It may be general.

Are different.
They can be same / may not be same as your loss function.

Can be different.
Accuracy. (not only this)

- Regression : -
Different evaluation.
Real number.
DIfferent story.

- Classification: -
Different evaluation
Probablities. DIfferent story


## Linear Regression in these 6 Jars.

- But first the intuition.
- How does it work ?
- Why does it work ?
- What are our assumptions if any ?

### Data Jar
- Real numbers.
- X_1.
- Predict Y.

### Model Jar: -
- X_1, Y_1. 
- Y_1 = m * X_1 + C

- Linear
- m,c need to be learnt.

### Task Jar
- Regression (Classifcation).
- Threshold = assumption.

### Learning Algorithm Jar
- Grandient Descent.
- Minimize the loss function.
- Calculate Gradients.
- Descent in opposite to gradient.
- Update m, c = 

m - m - grad(m)
c = c - grad(c)


- Squared error loss.

- NOT the only method NO.

- Close form solution.
Direct formula.
m, c optimal

### Cost/Loss Function Jar
- Squared Error loss (Regression)
- Distance minimization.

Gradients flow from loss function.

### Evaluation Jar
- Same loss function.
- Absolute error.

- R2 score (read about it)

0 - 1. 
Better the score better it does.
Goodness of fit.

