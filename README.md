# IEEE CS Data Science

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/oke-aditya/IEEE_CS_Data_Science/master)
[![HitCount](http://hits.dwyl.io/oke-aditya/IEEE_CS_Data_Science.svg)](http://hits.dwyl.io/oke-aditya/IEEE_CS_Data_Science)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/oke-aditya/IEEE_CS_Data_Science/pulls)


- Data Science Tutorial Sessions for IEEE CS Students.


Binder Link
https://mybinder.org/v2/gh/oke-aditya/IEEE_CS_Data_Science/master
