# Session 2

## Recap of Session 1.
### Theory
- Saw what is Data science, Machine Learning, DL, NLP, CV.
- Saw how Machine learning is divided into two techniques.
- Supervised and Unsupervised Learning
- Listed how supervised and unsupervised learning works.

### Hands-on
- Discussed about Python.
- Basics to Python Classes.
- Object oriented Programming
- Using specific functions such as eval(), map(), filter(), lambda(), enumerate()

## Session 1 Task assigned 
- Watch Linear regression in 1 variable videos.
- Revise theory, hands-on.
- Read Blogs.
- Fill missing stuff in hands-on.


## Session 2 Plan
### Theory
- Discuss Linear Regression in 1 variable. 
- Create intuition in multi-variate linear regression.
- Understand the 6 Jars Analogy.
- Understand Linear regression in 6 jars.

### Hands-on
- Intro to numpy, scikit-learn.
- Numpy arrays, broadcasting and etc.
- Intro to regression and using sklearn API for linear regression.

