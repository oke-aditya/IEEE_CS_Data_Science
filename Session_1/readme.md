# Introduction to Machine Learning and Data Science

## Topics to be covered
- Computational Thinking vs Statistical Thinking
- Thinking in Terms of Data Science
- Approach to be Developer vs Competitive Coder
- How to become a Data Scientist.


### What is ML ?
- Brief Overview of ML Techniques.
- Brief Overview of subjects related to ML.

## Hands-on Topics Covered

- Introduction to Python, Google Colaboratory.
- Introduction to Git, GitHub, Why GitHub ?
- Advanced Python Programming such as OOPS with Python.


## Tasks for Next Sessions
- Watching Video Lectures of Andrew Ng for Linear Regression.
- Practicing the Python Notebook shared.



